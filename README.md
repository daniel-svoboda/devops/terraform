# terraform_orchestration

Infrastructure Orchestration Automation with Terraform

## This is specifically shaped for Google Cloud integration
- Consult Terraform documentation and change cloud provider and other required components if needed

## Steps to follow:
- clone the repository
- add file `terraform.tfvars` where needed (most of dirs)
	- add all sensitive vars that should not be shared on git
	- e.g. `user_password	= ["pwd1234"]`
- amend list of variables according to your needs
- create terraform workspace if you want to have separate configs (e.g. staging/production)
- create account on Google Cloud
	- enable billing
	- create new project
	- setup terraform service account with rights to setup the entire infrastructure
	- save the service accounts `cred.json` into root terraform project you cloned
- within gcloud console enable admin apis required (e.g. Cloud SQL Admin API, Identity and Access Management, Cloud Resource Manager API, Kubernetes Engine API...)
- change name of your gcloud project in Dockerfile
- install/start local version of Docker
- run from root project folder `docker build -t terraform:v1 .` to build terraform package
- run/enter terraform container `docker run -it --rm terraform:v1 /bin/sh`
- from within terraform container setup the cloud infrastructure:
	- cd to each dir location you want to setup
	- run:
		- `terraform init`
		- `terraform plan`
		- `terraform apply`

IMPORTANT:
- store `terraform.tfstate` and `terraform.tfstate.backup` safely, its required for smooth future terraform runs and contains *highly sensitive information*


TODO: 
* store terraform.tfstate in consul?
