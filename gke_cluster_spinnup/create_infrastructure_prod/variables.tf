
variable "project" {
  type    = "string"
  default = "app-one-project"
}

variable "service-account-email" {
  type = "string"
  default = ""
}

variable "num_of_nodes" {
	type 	= "string"
	default = "1"
	description = "Number of nodes in each GKE cluster zone"
}

variable "additional_zones" {
  type = "list"
  default = []
}

variable "region" {
  type    = "string"
  default = "europe-west3"
}

variable "master_user" {
  description = "Username to authenticate with the k8s master"
}

variable "master_pass" {
  description = "Username to authenticate with the k8s master"
}

variable "instance_type" {
  type    = "string"
  default = "n1-standard-1"
}

variable "kubernetes_logging_service" {
  type    = "string"
  default = "logging.googleapis.com/kubernetes"
}

variable "kubernetes_monitoring_service" {
  type    = "string"
  default = "monitoring.googleapis.com/kubernetes"
}

