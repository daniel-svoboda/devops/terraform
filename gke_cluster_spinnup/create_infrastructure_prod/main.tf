
# Get latest cluster version
data "google_container_engine_versions" "europe-west3" {
  zone = "${var.region}-a"
  project = "${var.project}"
}

# Create the GKE cluster
resource "google_container_cluster" "cluster" {
  name    = "${terraform.workspace}"
  project = "${var.project}"
  zone    = "${var.region}-a"

  additional_zones = "${var.additional_zones}"

  initial_node_count = "${var.num_of_nodes}"

  min_master_version = "${data.google_container_engine_versions.europe-west3.latest_master_version}"
  node_version       = "${data.google_container_engine_versions.europe-west3.latest_node_version}"

  logging_service    = "${var.kubernetes_logging_service}"
  monitoring_service = "${var.kubernetes_monitoring_service}"

  master_auth {
    username = "${var.master_user}"
    password = "${var.master_pass}"
  }

  node_config {
    machine_type    = "${var.instance_type}"
    service_account = "${var.service-account-email}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    
    labels = {
      env = "${terraform.workspace}"
    }

    tags = ["${terraform.workspace}",]
  }
}

