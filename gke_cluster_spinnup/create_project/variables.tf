
variable "project" {
  type    = "string"
  default = "app-one-project"
}

variable "region" {
  type    = "string"
  default = "europe-west3"
}

variable "billing_account" {
  type = "string"
}

variable "org_id" {
  type = "string"
}
