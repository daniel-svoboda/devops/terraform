provider "google" {
  region  = "${var.region}"
  zone    = "${var.region}-a"
  project = ""
}

# Create the project
resource "google_project" "project" {
  name            = "${var.project}"
  project_id      = "${var.project}"
#  org_id          = "${var.org_id}"
  billing_account = "${var.billing_account}"
}

