# terraform_notes.sh

# This little script is useful for creating/reapplying terraform for both prod/staging environments
# Initial Authentication with regular user account with organization wide project-creator privileges
# needed if youre creating new project as service-accounts can only be created specific to project, not organization-wide

# Authentication
# gcloud auth application-default login

# export GOOGLE_CLOUD_KEYFILE_JSON="/root/.config/gcloud/application_default_credentials.json"



# create_project
	# terraform workspace new prod
	# terraform workspace new staging

terraform workspace select staging

terraform apply -auto-approve

terraform workspace select prod

terraform apply -auto-approve
