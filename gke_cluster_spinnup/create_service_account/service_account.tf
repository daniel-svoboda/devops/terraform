provider "google" {
  region  = "${var.region}"
  zone    = "${var.region}-a"
  project = "${var.project}"
}

# Create the ci/cd service account
resource "google_service_account" "service-account" {
  account_id   = "${var.service_account}"
  display_name = "Service Account for CI/CD"
  project      = "${var.project}"
}

# Create a service account key
resource "google_service_account_key" "service-account-key" {
  service_account_id = "${google_service_account.service-account.name}"
}

# Add the service account to the project
resource "google_project_iam_member" "service-account-to-project" {
  count   = "${length(var.service_account_iam_roles)}"
  project = "${var.project}"
  role    = "${element(var.service_account_iam_roles, count.index)}"
  member  = "serviceAccount:${google_service_account.service-account.email}"
}

# Enable required services on the project
resource "google_project_service" "project-services" {
  count   = "${length(var.project_services)}"
  project = "${var.project}"
  service = "${element(var.project_services, count.index)}"

  # Do not disable the service on destroy. On destroy, we are going to
  # destroy the project, but we need the APIs available to destroy the
  # underlying resources.
  #        disable_on_destroy = false
}

output "service-account-email" {
  value       = "${google_service_account.service-account.email}"
}
