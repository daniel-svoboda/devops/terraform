
variable "project" {
  type    = "string"
  default = "app-one-project"
}

variable "region" {
  type    = "string"
  default = "europe-west3"
}

variable service_account {
  type = "string"
  default = "app-one-backend-db-access"
}

variable "service_account_iam_roles" {
  type = "list"

  default = [
    "roles/iam.serviceAccountUser",
    "roles/cloudsql.client"
  ]
}

variable "project_services" {
  type = "list"

  default = [
    "sql-component.googleapis.com",
    "cloudapis.googleapis.com",
    "sqladmin.googleapis.com"
  ]
}
