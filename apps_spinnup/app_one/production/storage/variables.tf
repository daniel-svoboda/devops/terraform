
# Database instance (VM) variables:

variable project {
  description = "Project name"
  default     = "app-one-project"
}

variable name {
  description = "Bucket name"
  default     = "app-one-prod-storage"
}

variable location {
  description = "Location"
  default     = "europe-west3"
}

variable storage_class {
  description = "Supported values MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE."
  default     = "REGIONAL"
}
