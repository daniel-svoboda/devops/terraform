# Create Cloud SQL VM
resource "google_storage_bucket" "storage" {
  project  = "${var.project}"
  name     = "${var.name}"
  location = "${var.location}"
  storage_class = "${var.storage_class}"

}

# Output info:
output "self_link" {
  value = "${google_storage_bucket.storage.self_link}"
}

output "url" {
  value = "${google_storage_bucket.storage.url}"
}
