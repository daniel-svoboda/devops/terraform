
# Database instance (VM) variables:

variable vm_name {
  description = "Name for the database instance. Must be unique and cannot be reused for up to one week."
  default     = "app-one-prod"
}

variable project {
  description = "Project name"
  default     = "app-one-project"
}

variable database_version {
  description = "Database version to use (SQL or PostgreSQL)"
  default     = "POSTGRES_9_6"
}

variable region {
  description = "region to build in"
  default     = "europe-west3"
}

variable zone {
  description = "zone to build in"
  default     = "europe-west3-a"
}

variable tier {
  description = "The machine tier (type (Second Generation), more info on https://cloud.google.com/sql/pricing"
  default     = "db-f1-micro"
}

variable availability_type {
  description = "This specifies whether a PostgreSQL instance should be set up for high availability (REGIONAL) or single zone (ZONAL)"
  default     = "ZONAL"
}

variable disk_size {
  description = "Second generation only. The size of data disk, in GB. Size of a running instance cannot be reduced but can be increased."
  default     = 10
}

variable disk_type {
  description = "The type of data disk either `PD_SSD` or `PD_HDD`."
  default     = "PD_SSD"
}
variable disk_autoresize {
  description = "Second Generation only. Configuration to increase storage size automatically."
  default     = true
}

variable backup_enabled {
  description = "Enable/Disable auto-backup"
  default     = true
}

variable backup_time {
  description = "Auto-backup start time"
  default     = "01:00"
}

variable maintenance_day {
  description = "On which day can google do updates 1-Monday - 7-Sunday"
  default     = 7
}

variable maintenance_hour {
  description = "On which hour can google do updates 0-23 UTC"
  default     = 5
}

# Databases setup variables

variable db_name {
  description = "Name of the default database to create"
  type        = "list"
  default     = ["app-one-db", "kong"]
}


variable user_name {
  description = "The name of the default user"
  type        = "list"
  default     = ["app-one-db", "kong"]
}

variable user_password {
  description = "The password for the default user. If not set, a random one will be generated and available in the generated_user_password output variable."
  type        = "list"
}

