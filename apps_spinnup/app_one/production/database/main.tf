# Create Cloud SQL VM
resource "google_sql_database_instance" "master" {
  name = "${var.vm_name}"
  project = "${var.project}"
  database_version = "${var.database_version}"
  region = "${var.region}"

  settings {
    tier = "${var.tier}"
    availability_type = "${var.availability_type}"
    disk_size = "${var.disk_size}"
    disk_type = "${var.disk_type}"
    disk_autoresize = "${var.disk_autoresize}"    
    location_preference {
      zone = "${var.zone}"
    }
    backup_configuration {
      enabled = "${var.backup_enabled}"
      start_time = "${var.backup_time}"
    }
    maintenance_window {
      day = "${var.maintenance_day}"
      hour = "${var.maintenance_hour}"
    }
  }

  timeouts {
    create = "20m"
    delete = "20m"
  }  
}

# Create Databases
resource "google_sql_database" "default" {
  count     = "${length(var.db_name)}"
  name      = "${var.db_name[count.index]}"
  project   = "${var.project}"
  instance  = "${google_sql_database_instance.master.name}"
}

# Create database users
resource "google_sql_user" "default" {
  count    = "${length(var.user_name)}"  
  name     = "${var.user_name[count.index]}"
  project  = "${var.project}"
  instance = "${google_sql_database_instance.master.name}"
  password = "${var.user_password[count.index]}"
}

# Output info:
output "self_link" {
  value = "${google_sql_database_instance.master.self_link}"
}

output "connection_name" {
  value = "${google_sql_database_instance.master.connection_name}"
}

